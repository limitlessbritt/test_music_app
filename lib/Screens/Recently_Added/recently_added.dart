import 'package:flutter/material.dart';
import 'package:test_music_app/API/api_call.dart';

import 'package:test_music_app/API/auth.dart';

class RecentlyAddedAlbums extends StatefulWidget {
  @override
  _RecentlyAddedAlbumsState createState() => _RecentlyAddedAlbumsState();
}

class _RecentlyAddedAlbumsState extends State<RecentlyAddedAlbums> {
  Future<List<Album>> albums;
  List<String> coverARTLIST = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* backgroundColor: Colors.black, */
      body: Container(
        child: FutureBuilder(
            future: fetchRecentlyAddedAlbums(),
            builder: (context, AsyncSnapshot<List<Album>> data) {
              switch (data.connectionState) {
                case ConnectionState.none:
                  return Text(
                    "none",
                    style: TextStyle(color: Colors.black),
                  );
                case ConnectionState.waiting:
                  return Center(
                      child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                  ));
                case ConnectionState.active:
                  return Text('');
                case ConnectionState.done:
                  if (data.hasData) {
                    List<Album> albums = data.data;
/*                     print("${albums.length}"); */
                    return ListView.builder(
                      itemCount: albums.length,
                      itemBuilder: (context, index) {
                        return FutureBuilder(
                            future: recentAlbumArt(albums[index].coverArt),
                            builder: (context, AsyncSnapshot cover) {
                              switch (cover.connectionState) {
                                case ConnectionState.none:
                                  return Text(
                                    "none",
                                    style: TextStyle(color: Colors.black),
                                  );
                                case ConnectionState.waiting:
                                  return Center(
                                      child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.black),
                                  ));
                                case ConnectionState.active:
                                  return Text('');
                                case ConnectionState.done:
                                  if (cover.hasData) {
/*                                     List<String> coverARTLIST = cover.data; */
                                    coverARTLIST.add(cover.data);
                                    albumART(length) {
                                      List<Container> containers =
                                          List<Container>.generate(length,
                                              (index) {
                                        return Container(
                                            child: Image.network(
                                                coverARTLIST[index]));
                                      });
                                      return containers;
                                    }

                                    return GridView.count(
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        crossAxisCount: 2,
                                        padding: EdgeInsets.all(2),
                                        children:
                                            albumART(coverARTLIST.length));
                                  }
                              }
                            });
                      },
                    );
                  }
              }
            }),
      ),
    );
  }
}
