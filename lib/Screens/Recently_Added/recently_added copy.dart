import 'package:flutter/material.dart';
import 'package:test_music_app/API/api_call.dart';

import 'package:test_music_app/API/auth.dart';

class RecentlyAddedAlbums extends StatefulWidget {
  @override
  _RecentlyAddedAlbumsState createState() => _RecentlyAddedAlbumsState();
}

class _RecentlyAddedAlbumsState extends State<RecentlyAddedAlbums> {
  Future<List<Album>> albums;
  List<String> coverARTLIST = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* backgroundColor: Colors.black, */
      body: Container(
        child: FutureBuilder(
            future: fetchRecentlyAddedAlbums(),
            builder: (context, AsyncSnapshot<List<Album>> data) {
              switch (data.connectionState) {
                case ConnectionState.none:
                  return Text(
                    "none",
                    style: TextStyle(color: Colors.black),
                  );
                case ConnectionState.waiting:
                  return Center(
                      child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                  ));
                case ConnectionState.active:
                  return Text('');
                case ConnectionState.done:
                  if (data.hasData) {
                    List<Album> albums = data.data;
                    print("${albums.length}");
                    return ListView.builder(
                      itemCount: albums.length,
                      itemBuilder: (context, index) {
                        return FutureBuilder(
                            future: recentAlbumArt(albums[index].coverArt),
                            builder: (context, AsyncSnapshot cover) {
                              switch (cover.connectionState) {
                                case ConnectionState.none:
                                  return Text(
                                    "none",
                                    style: TextStyle(color: Colors.black),
                                  );
                                case ConnectionState.waiting:
                                  return Center(
                                      child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.black),
                                  ));
                                case ConnectionState.active:
                                  return Text('');
                                case ConnectionState.done:
                                  if (cover.hasData) {
                                    coverARTLIST.add(cover.data);
                                    print("${coverARTLIST.length}");
                                    return GridView.count(
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      crossAxisCount: 2,
                                      children: <Widget>[
                                        ListView.builder(
                                            itemCount: coverARTLIST.length,
                                            itemBuilder: (context, index) {
                                              GridTile(
                                                  child: Image.network(
                                                      coverARTLIST[1]));
                                            })
                                      ],
                                    );
                                  }
                              }
                            });
                      },
                    );
                  }
              }
            }),
      ),
    );
  }
}

getCoverArt(){
  uRL = recentAlbumArt(albums[index].coverArt),
}