import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tape-Out',
      theme: ThemeData(
        primaryColor: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Tape-Out'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  void itemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Colors.black,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              color: Colors.white,
              onPressed: () {
                print("setting");
              }),
        ],
      ),
      body: Center(
        child: ListView(
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            Container(
              alignment: Alignment.topLeft,
              width: 500.0,
              height: 60.0,
              padding: EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  Get.toNamed('/recentlyadded');
                },
                child: new Text(
                  "Recently Added",
                  style: new TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
            Container(
                alignment: Alignment.topLeft,
                width: 500.0,
                height: 150.0,
                padding: EdgeInsets.all(5.0),
                child: Card()),
            Container(
              alignment: Alignment.topLeft,
              width: 500.0,
              height: 60.0,
              padding: EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  print("tapped");
                },
                child: new Text(
                  "Recently Played",
                  style: new TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
            Container(
                alignment: Alignment.topLeft,
                width: 500.0,
                height: 150.0,
                padding: EdgeInsets.all(5.0),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Image(
                        image: NetworkImage(
                            "https://upload.wikimedia.org/wikipedia/en/7/7e/Dave_East_-_Survival.png"),
                        alignment: Alignment.centerLeft),
                    Image(
                      image: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/en/f/ff/Nicki_Minaj_-_Queen.png"),
                      alignment: Alignment.centerLeft,
                    ),
                    Image(
                      image: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/en/1/11/Dive_tycho_album.jpg"),
                      alignment: Alignment.centerLeft,
                    ),
                    Image(
                      image: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/en/4/46/Nipsey_Hussle_%E2%80%93_Victory_Lap.png"),
                      alignment: Alignment.centerLeft,
                    ),
                    Image(
                      image: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/en/e/e9/Megan_Thee_Stallion_SUGA_Cover.jpeg"),
                      alignment: Alignment.centerLeft,
                    ),
                  ],
                )),
            Container(
              alignment: Alignment.topLeft,
              width: 500.0,
              height: 60.0,
              padding: EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  print("TARPPED");
                },
                child: new Text(
                  "Random Albums",
                  style: new TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              width: 500.0,
              height: 150.0,
              padding: EdgeInsets.all(5.0),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Image(
                      image: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/en/7/7e/Dave_East_-_Survival.png"),
                      alignment: Alignment.centerLeft),
                  Image(
                    image: NetworkImage(
                        "https://upload.wikimedia.org/wikipedia/en/f/ff/Nicki_Minaj_-_Queen.png"),
                    alignment: Alignment.centerLeft,
                  ),
                  Image(
                    image: NetworkImage(
                        "https://upload.wikimedia.org/wikipedia/en/1/11/Dive_tycho_album.jpg"),
                    alignment: Alignment.centerLeft,
                  ),
                  Image(
                    image: NetworkImage(
                        "https://upload.wikimedia.org/wikipedia/en/4/46/Nipsey_Hussle_%E2%80%93_Victory_Lap.png"),
                    alignment: Alignment.centerLeft,
                  ),
                  Image(
                    image: NetworkImage(
                        "https://upload.wikimedia.org/wikipedia/en/e/e9/Megan_Thee_Stallion_SUGA_Cover.jpeg"),
                    alignment: Alignment.centerLeft,
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              width: 500.0,
              height: 300.0,
              child: ListView(
                children: <Widget>[
                  Divider(
                    indent: 5,
                    height: 1,
                    color: Colors.grey,
                  ),
                  ListTile(
                    title: Text("Songs",
                        style:
                            new TextStyle(color: Colors.white, fontSize: 18)),
                    onTap: () {
                      print("songs");
                    },
                  ),
                  Divider(
                    indent: 5,
                    height: 0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    title: Text("Artists",
                        style:
                            new TextStyle(color: Colors.white, fontSize: 18)),
                  ),
                  Divider(
                    indent: 5,
                    height: 0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    title: Text("Albums",
                        style:
                            new TextStyle(color: Colors.white, fontSize: 18)),
                    onTap: () {
                      print("tapped");
                    },
                  ),
                  Divider(
                    indent: 5,
                    height: 0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    title: Text("Favorites",
                        style:
                            new TextStyle(color: Colors.white, fontSize: 18)),
                    onTap: () {
                      Get.toNamed('/loved');
                    },
                  ),
                  Divider(
                    indent: 5,
                    height: 0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    title: Text("Genres",
                        style:
                            new TextStyle(color: Colors.white, fontSize: 18)),
                    onTap: () {
                      print("tapped");
                    },
                  ),
                  Divider(
                    indent: 5,
                    height: 0,
                    color: Colors.grey,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.white,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.library_music), title: Text("Library")),
          BottomNavigationBarItem(
              icon: Icon(Icons.search), title: Text("Search")),
          BottomNavigationBarItem(
              icon: Icon(Icons.playlist_play), title: Text("Playlists")),
        ],
        currentIndex: _selectedIndex,
        onTap: itemTapped,
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
