import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_music_app/API/auth.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

String server;
String password;
String username;

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController serverController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController usernameController = new TextEditingController();

  void initState() {
    super.initState();
    passwordController.addListener(() {
      password = passwordController.text;
    });
    serverController.addListener(() {
      server = serverController.text;
    });
    usernameController.addListener(() {
      username = usernameController.text;
    });
  }

  void dispose() {
    passwordController.dispose();
    serverController.dispose();
    usernameController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 50.00),
                width: double.infinity,
                height: 80.00,
                child: Icon(
                  Icons.music_note,
                  size: 100.00,
                  color: Colors.white,
                ),
              ),
              Container(
                height: 60.00,
                width: double.infinity,
                margin: EdgeInsets.only(top: 30.00),
                child: Text(
                  "Welcome to Tape-Out",
                  style: TextStyle(fontSize: 30.00, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                height: 30.00,
                width: double.infinity,
                margin: EdgeInsets.only(top: 1.00),
                child: Text(
                  "Please connet to your music server.",
                  style: TextStyle(fontSize: 20.00, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                height: 30.00,
                width: double.infinity,
                margin: EdgeInsets.only(top: 5.00),
                child: Text(
                  "Server Address:",
                  style: TextStyle(fontSize: 15.00, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 5.00),
                  child: TextField(
                    obscureText: false,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    decoration: InputDecoration(
                        hintText: 'http://',
                        hintStyle:
                            TextStyle(fontSize: 20, color: Colors.grey[850]),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 2,
                            )),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 3,
                            )),
                        prefix: Padding(
                          child: IconTheme(
                            data: IconThemeData(color: Colors.grey[850]),
                            child: Icon(Icons.dns),
                          ),
                          padding: EdgeInsets.only(left: 30, right: 10),
                        )),
                    controller: serverController,
                  )),
              Container(
                height: 30.00,
                width: double.infinity,
                margin: EdgeInsets.only(top: 15.00),
                child: Text(
                  "Username:",
                  style: TextStyle(fontSize: 15.00, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 5.00),
                  child: TextField(
                    obscureText: false,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 2,
                            )),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 3,
                            )),
                        prefix: Padding(
                          child: IconTheme(
                            data: IconThemeData(color: Colors.grey[850]),
                            child: Icon(Icons.perm_identity),
                          ),
                          padding: EdgeInsets.only(left: 30, right: 10),
                        )),
                    controller: usernameController,
                  )),
              Container(
                height: 30.00,
                width: double.infinity,
                margin: EdgeInsets.only(top: 15.00),
                child: Text(
                  "Password:",
                  style: TextStyle(fontSize: 15.00, color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 5.00),
                  child: TextField(
                    obscureText: true,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 2,
                            )),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(
                              color: Colors.grey[850],
                              width: 3,
                            )),
                        prefix: Padding(
                          child: IconTheme(
                            data: IconThemeData(color: Colors.grey[850]),
                            child: Icon(Icons.security),
                          ),
                          padding: EdgeInsets.only(left: 30, right: 10),
                        )),
                    controller: passwordController,
                  )),
              Container(
                height: 30.00,
                margin: EdgeInsets.only(top: 10.00),
                child: RaisedButton(
                  color: Colors.black,
                  child: Text(
                    "Connect",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 20),
                  ),
                  onPressed: () {
                    // TODO: Progress bar when useer clicks login button.
                    userlogin(context);
                  },
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

showAlertDialogUP(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Error"),
    content: Text("Wrong credentials. Please try again."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showAlertDialogS(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Error"),
    content: Text("Couldn't connect to server. Please Try Again."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

//TODO: Tell user if app cannot connect to server

userlogin(context) {
  fetchResponse().then((response) {
    if (response.status != null) {
      switch (response.status) {
        case "ok":
          {
            Get.toNamed('/home');
          }
          break;
        case "failed":
          {
            if (response.error.code == 40) {
              return showAlertDialogUP(context);
            }
            if (response.error.code == 30) {
              return serverNeedUpdate(context);
            } else {
              return showAlertDialogS(context);
            }
          }
          break;
      }
    } else {
      return showAlertDialogS(context);
    }
  });
}

serverNeedUpdate(BuildContext context) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Error"),
    content: Text("Couldn't connect to server. Please Try Again."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
//TODO: Store User info
