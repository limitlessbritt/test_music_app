import 'dart:convert';

Apicall apicallFromJson(Map<String, dynamic> str) => Apicall.fromJson(str);

String apicallToJson(Apicall data) => json.encode(data.toJson());

class Apicall {
  Apicall({
    this.subsonicResponse,
  });

  SubsonicResponse subsonicResponse;

  factory Apicall.fromJson(Map<String, dynamic> json) => Apicall(
        subsonicResponse: json["subsonic-response"] == null
            ? null
            : SubsonicResponse.fromJson(json["subsonic-response"]),
      );

  Map<String, dynamic> toJson() => {
        "subsonic-response":
            subsonicResponse == null ? null : subsonicResponse.toJson(),
      };
}

class SubsonicResponse {
  SubsonicResponse({
    this.status,
    this.version,
    this.albumList,
    this.error,
    this.user,
    this.starred2,
  });

  String status;
  String version;
  AlbumList albumList;
  User user;
  Error error;
  Starred2 starred2;

  factory SubsonicResponse.fromJson(Map<String, dynamic> json) =>
      SubsonicResponse(
        status: json["status"] == null ? null : json["status"],
        version: json["version"] == null ? null : json["version"],
        error: json["error"] == null ? null : Error.fromJson(json["error"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        starred2: json["starred2"] == null
            ? null
            : Starred2.fromJson(json["starred2"]),
        albumList: json["albumList"] == null
            ? null
            : AlbumList.fromJson(json["albumList"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "error": error == null ? null : error.toJson(),
        "version": version == null ? null : version,
        "starred2": starred2 == null ? null : starred2.toJson(),
        "albumList": albumList == null ? null : albumList.toJson(),
      };
}

class User {
  User({
    this.username,
    this.email,
    this.scrobblingEnabled,
    this.adminRole,
    this.settingsRole,
    this.downloadRole,
    this.uploadRole,
    this.playlistRole,
    this.coverArtRole,
    this.commentRole,
    this.podcastRole,
    this.streamRole,
    this.jukeboxRole,
    this.shareRole,
    this.videoConversionRole,
    this.folder,
  });

  final String username;
  String email;
  bool scrobblingEnabled;
  bool adminRole;
  bool settingsRole;
  bool downloadRole;
  bool uploadRole;
  bool playlistRole;
  bool coverArtRole;
  bool commentRole;
  bool podcastRole;
  bool streamRole;
  bool jukeboxRole;
  bool shareRole;
  bool videoConversionRole;
  List<int> folder;

  factory User.fromJson(Map<String, dynamic> json) => User(
        username: json["username"] == null ? null : json["username"],
        email: json["email"] == null ? null : json["email"],
        scrobblingEnabled: json["scrobblingEnabled"] == null
            ? null
            : json["scrobblingEnabled"],
        adminRole: json["adminRole"] == null ? null : json["adminRole"],
        settingsRole:
            json["settingsRole"] == null ? null : json["settingsRole"],
        downloadRole:
            json["downloadRole"] == null ? null : json["downloadRole"],
        uploadRole: json["uploadRole"] == null ? null : json["uploadRole"],
        playlistRole:
            json["playlistRole"] == null ? null : json["playlistRole"],
        coverArtRole:
            json["coverArtRole"] == null ? null : json["coverArtRole"],
        commentRole: json["commentRole"] == null ? null : json["commentRole"],
        podcastRole: json["podcastRole"] == null ? null : json["podcastRole"],
        streamRole: json["streamRole"] == null ? null : json["streamRole"],
        jukeboxRole: json["jukeboxRole"] == null ? null : json["jukeboxRole"],
        shareRole: json["shareRole"] == null ? null : json["shareRole"],
        videoConversionRole: json["videoConversionRole"] == null
            ? null
            : json["videoConversionRole"],
        folder: json["folder"] == null
            ? null
            : List<int>.from(json["folder"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "username": username == null ? null : username,
        "email": email == null ? null : email,
        "scrobblingEnabled":
            scrobblingEnabled == null ? null : scrobblingEnabled,
        "adminRole": adminRole == null ? null : adminRole,
        "settingsRole": settingsRole == null ? null : settingsRole,
        "downloadRole": downloadRole == null ? null : downloadRole,
        "uploadRole": uploadRole == null ? null : uploadRole,
        "playlistRole": playlistRole == null ? null : playlistRole,
        "coverArtRole": coverArtRole == null ? null : coverArtRole,
        "commentRole": commentRole == null ? null : commentRole,
        "podcastRole": podcastRole == null ? null : podcastRole,
        "streamRole": streamRole == null ? null : streamRole,
        "jukeboxRole": jukeboxRole == null ? null : jukeboxRole,
        "shareRole": shareRole == null ? null : shareRole,
        "videoConversionRole":
            videoConversionRole == null ? null : videoConversionRole,
        "folder":
            folder == null ? null : List<dynamic>.from(folder.map((x) => x)),
      };
}

class Error {
  Error({
    this.code,
    this.message,
  });

  int code;
  String message;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        code: json["code"] == null ? null : json["code"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "message": message == null ? null : message,
      };
}

class Starred2 {
  Starred2({
    this.song,
  });

  List<Song> song;

  factory Starred2.fromJson(Map<String, dynamic> json) => Starred2(
        song: json["song"] == null
            ? null
            : List<Song>.from(json["song"].map((x) => Song.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "song": song == null
            ? null
            : List<dynamic>.from(song.map((x) => x.toJson())),
      };
}

class Song {
  Song({
    this.id,
    this.parent,
    this.isDir,
    this.title,
    this.album,
    this.artist,
    this.track,
    this.genre,
    this.coverArt,
    this.size,
    this.contentType,
    this.suffix,
    this.duration,
    this.bitRate,
    this.path,
    this.isVideo,
    this.playCount,
    this.created,
    this.starred,
    this.albumId,
    this.artistId,
    this.type,
    this.year,
  });

  String id;
  String parent;
  bool isDir;
  String title;
  String album;
  String artist;
  int track;
  String genre;
  String coverArt;
  int size;
  String contentType;
  String suffix;
  int duration;
  int bitRate;
  String path;
  bool isVideo;
  int playCount;
  DateTime created;
  DateTime starred;
  String albumId;
  String artistId;
  String type;
  int year;

  factory Song.fromJson(Map<String, dynamic> json) => Song(
        id: json["id"] == null ? null : json["id"],
        parent: json["parent"] == null ? null : json["parent"],
        isDir: json["isDir"] == null ? null : json["isDir"],
        title: json["title"] == null ? null : json["title"],
        album: json["album"] == null ? null : json["album"],
        artist: json["artist"] == null ? null : json["artist"],
        track: json["track"] == null ? null : json["track"],
        genre: json["genre"] == null ? null : json["genre"],
        coverArt: json["coverArt"] == null ? null : json["coverArt"],
        size: json["size"] == null ? null : json["size"],
        contentType: json["contentType"] == null ? null : json["contentType"],
        suffix: json["suffix"] == null ? null : json["suffix"],
        duration: json["duration"] == null ? null : json["duration"],
        bitRate: json["bitRate"] == null ? null : json["bitRate"],
        path: json["path"] == null ? null : json["path"],
        isVideo: json["isVideo"] == null ? null : json["isVideo"],
        playCount: json["playCount"] == null ? null : json["playCount"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        starred:
            json["starred"] == null ? null : DateTime.parse(json["starred"]),
        albumId: json["albumId"] == null ? null : json["albumId"],
        artistId: json["artistId"] == null ? null : json["artistId"],
        type: json["type"] == null ? null : json["type"],
        year: json["year"] == null ? null : json["year"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "parent": parent == null ? null : parent,
        "isDir": isDir == null ? null : isDir,
        "title": title == null ? null : title,
        "album": album == null ? null : album,
        "artist": artist == null ? null : artist,
        "track": track == null ? null : track,
        "genre": genre == null ? null : genre,
        "coverArt": coverArt == null ? null : coverArt,
        "size": size == null ? null : size,
        "contentType": contentType == null ? null : contentType,
        "suffix": suffix == null ? null : suffix,
        "duration": duration == null ? null : duration,
        "bitRate": bitRate == null ? null : bitRate,
        "path": path == null ? null : path,
        "isVideo": isVideo == null ? null : isVideo,
        "playCount": playCount == null ? null : playCount,
        "created": created == null ? null : created.toIso8601String(),
        "starred": starred == null ? null : starred.toIso8601String(),
        "albumId": albumId == null ? null : albumId,
        "artistId": artistId == null ? null : artistId,
        "type": type == null ? null : type,
        "year": year == null ? null : year,
      };
}

class AlbumList {
  AlbumList({
    this.album,
  });

  List<Album> album;

  factory AlbumList.fromJson(Map<String, dynamic> json) => AlbumList(
        album: json["album"] == null
            ? null
            : List<Album>.from(json["album"].map((x) => Album.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "album": album == null
            ? null
            : List<dynamic>.from(album.map((x) => x.toJson())),
      };
}

class Album {
  Album({
    this.id,
    this.parent,
    this.isDir,
    this.title,
    this.album,
    this.artist,
    this.genre,
    this.coverArt,
    this.playCount,
    this.created,
    this.year,
  });

  String id;
  String parent;
  bool isDir;
  String title;
  String album;
  String artist;
  String genre;
  String coverArt;
  int playCount;
  DateTime created;
  int year;

  factory Album.fromJson(Map<String, dynamic> json) => Album(
        id: json["id"] == null ? null : json["id"],
        parent: json["parent"] == null ? null : json["parent"],
        isDir: json["isDir"] == null ? null : json["isDir"],
        title: json["title"] == null ? null : json["title"],
        album: json["album"] == null ? null : json["album"],
        artist: json["artist"] == null ? null : json["artist"],
        genre: json["genre"] == null ? null : json["genre"],
        coverArt: json["coverArt"] == null ? null : json["coverArt"],
        playCount: json["playCount"] == null ? null : json["playCount"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
        year: json["year"] == null ? null : json["year"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "parent": parent == null ? null : parent,
        "isDir": isDir == null ? null : isDir,
        "title": title == null ? null : title,
        "album": album == null ? null : album,
        "artist": artist == null ? null : artist,
        "genre": genre == null ? null : genre,
        "coverArt": coverArt == null ? null : coverArt,
        "playCount": playCount == null ? null : playCount,
        "created": created == null ? null : created.toIso8601String(),
        "year": year == null ? null : year,
      };
}
