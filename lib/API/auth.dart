import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';
import 'package:crypto/crypto.dart';
import 'package:test_music_app/API/api_call.dart';
import 'package:test_music_app/Screens/Login/login_screen.dart';

String rest = "/rest/";
String tapeOutVerison = "1.13.0";
String client = "TapeOut";
/* String salt = randomToken(6); */
/* String token = makeToken("$password", "$salt"); */
String format = "&f=json";
String ping = "ping";

final _random = Random();
// Salt
String randomToken(int length) => String.fromCharCodes(
      List.generate(length, (_) {
        var ch = _random.nextInt(52);
        if (ch > 25) {
          ch += 6;
        }
        return ch + 0x41;
      }),
    );

String newSalt() => randomToken(6);

// Token
String makeToken(String password, String salt) =>
    md5.convert(utf8.encode(password + salt)).toString().toLowerCase();

//TODO: catch server error and let user know
Future<SubsonicResponse> fetchResponse() async {
  try {
    var salt = randomToken(6);
    var token = makeToken("$password", "$salt");
    var authURL =
        "$server/rest/ping?u=$username&t=$token&s=$salt&v=$tapeOutVerison&c=$client$format";
    var authresponse = await http.get(authURL);
    if (authresponse.statusCode == 200) {
      var jsondata = jsonDecode(authresponse.body);
      var data = apicallFromJson(jsondata);
      var response = data.subsonicResponse;
      return response;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

Future<List<Album>> fetchRecentlyAddedAlbums() async {
  try {
    var salt = randomToken(6);
    var token = makeToken("$password", "$salt");
    var uRL =
        "$server/rest/getAlbumList?u=$username&t=$token&s=$salt&v=$tapeOutVerison&c=$client$format&type=newest";
    var authresponse = await http.get(uRL);
    if (authresponse.statusCode == 200) {
      var jsondata = jsonDecode(authresponse.body);
      var data = apicallFromJson(jsondata);
      var aresponse = data.subsonicResponse.albumList.album;
      return aresponse;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

Future recentAlbumArt(String coverArtID) async {
  try {
    var salt = randomToken(6);
    var token = makeToken("$password", "$salt");
    var uRL =
        "$server/rest/getCoverArt/?u=$username&t=$token&s=$salt&v=$tapeOutVerison&c=$client$format&id=$coverArtID";
    return uRL.toString();
  } catch (e) {
    print(e);
  }
}

class COVERART {
  COVERART({this.coverURL});
  String coverURL;
}
