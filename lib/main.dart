import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:test_music_app/Screens/Login/login_screen.dart';
import 'Screens/Home/home_screen.dart';
import 'Screens/Recently_Added/recently_added.dart';

void main() {
  runApp(GetMaterialApp(
    initialRoute: '/',
    namedRoutes: {
      '/': GetRoute(page: LoginScreen()),
      '/home': GetRoute(page: Home()),
      '/recentlyadded': GetRoute(page: RecentlyAddedAlbums()),
    },
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(),
    );
  }
}
