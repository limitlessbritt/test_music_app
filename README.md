# Music App Test

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
![Flutter CI](https://github.com/limitlessbritt/test_music_app/workflows/Flutter%20CI/badge.svg)
[![GitHub release](https://img.shields.io/github/release/Naereen/StrapDown.js.svg)](https://github.com/limitlessbritt/Tape-Out/releases)
[![GitHub issues](https://img.shields.io/github/issues/Naereen/StrapDown.js.svg)](https://github.com/limitlessbritt/Tape-Out/issues/)
[![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![CodeFactor](https://www.codefactor.io/repository/github/limitlessbritt/test_music_app/badge?s=2ea4db077230f0c597a3a371bd9dec9c124df853)](https://www.codefactor.io/repository/github/limitlessbritt/test_music_app)

This is a moblie client app for Subsonic API compatible servers.
Written in Flutter
Tested with Navidrome & Airsonic

## Why?

I wanted something modern looking that also combines features from multiple apps to make the app in one app.

## Features

- Album View

## Installation

Download the app via the PlayStore, f-droid or in the releases page.

## To Do

ALOT

## Contributions

## Acknowledgements

## License
